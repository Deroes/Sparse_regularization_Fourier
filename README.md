# Sparse_regularization_Fourier: A sparse regularization framework for Fourier-based plane wave imaging
This package contains the Matlab codes related to the sparse regularization methods presented in the paper:
A. Besson, M. Zhang, F. Varray, H. Liebgott, D. Friboulet, Y. Wiaux, J.-P. Thiran, R. E. Carrillo and O. Bernard, A sparse regularization framework for Fourier-based plane wave beamforming, to appear in IEEE UFFC

## Content
The main directories:
- /src code directory which contains
    * ADMM folder: source codes for the optimization algorithm (from UnlocBox)
    * misc: source codes for various function: evaluation of the contrast, computation of the log-compressed B-mode image...
    * reconstruction_methods: source codes for the 3 Fourier-based reconstruction methods (classic and sparse): UFSB, fkmig and lumig
    * sparsity_model: source codes for forward and backward orthogonal and undecimated wavelet trasnform
- /irt Fessler image reconstruction toolbox (http://web.eecs.umich.edu/~fessler/code/index.html)
- /data contains the raw data (simulated anechoic phantom with FIeld II software)
The main scripts:
- script_demo_classic.m which runs the classical methods on the anechoic phantom
- script_demo_classic.m which runs the sparse regularization methods on the anechoic phantom

## Credits
- The nufft algorithm comes from Fessler's image reconstruction toolbox (http://web.eecs.umich.edu/~fessler/code/index.html).
- The classical fkmig and lumig methods come from Damien Garcia's website (http://www.biomecardio.com/pageshtm/tools/toolsen.htm). 
- The classical ufsb method has been implemented by Olivier Bernard (https://www.creatis.insa-lyon.fr/~bernard/).
- Some functions of the optimization algorithm comes from the UnlocBox toolbox developed by LTS2 at EPFL (https://lts2.epfl.ch/unlocbox/).

## Reference
The code is free and you can use it for your own research. Please cite the paper: "A sparse regularization framework for Fourier-based plane wave imaging".

## Copyright
Copyright (c) 2016, Adrien Besson, Rafael Carrillo, Olivier Bernard.