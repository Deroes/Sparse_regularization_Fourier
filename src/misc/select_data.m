function [rf_data, probe] = select_data(dataRef, AngleRef, number_plane_waves, probe)
rf_data = [];
time = zeros(1, number_plane_waves);

if number_plane_waves ==1
    steeredAngle = 0;
    idx = AngleRef==steeredAngle;
    rf_data = dataRef{idx}.rawdata;
    time = dataRef{idx}.dt;
else
    num = (number_plane_waves-1)/2;
    steeredAngle = (-num:num);  % in degree
    for k=1:number_plane_waves
        idx = AngleRef==steeredAngle(k);
        v = dataRef{idx}.rawdata;
        rf_data(1:size(v,1),:,k) = v;
        time(k) = dataRef{idx}.dt;
    end
end
probe.t0 = time;
probe.TXangle = deg2rad(steeredAngle);


end