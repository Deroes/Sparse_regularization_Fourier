function [x,z,bmode,rf_gridding,kx,kz] = UFSB(SIG, param)

%UFSB - Perform Ultrasound Fourier Slice Beamforming (UFSB) to reconstruct US images
%
% Syntax:  [bmode,migSIG,param] = ufsb(SIG,param)
%
% Inputs:
%    SIG - element raw data (stored as a 3D matrix (number of lines * number of transducers * number of frames))
%    param - structure containing various parameter related to the probe, medium (cf. lumig.m for more details)
%			- param.settings contains details specific to UFSB:
%				- settings.ximax: Maximum steering angle simulated in receive
%				- settings.dxi: Angle step for simulated angles
% Outputs:
%    bmode - bmode image (normalized envelope)
%    migSIG - RF image
%	 param - structure which contains details related to the image, medium and probe
%
%
% Other m-files required: Fessler's irt toolbox needed (for the non-uniform Fourier transform) 
% Subfunctions: none
%
% See also: lumig
% Reference: O. Bernard, M. Zhang, F. Varray, P. Gueth, J.-Ph. Thiran, H. Liebgott, and D. Friboulet, 'Ultrasound Fourier Slice Imaging: a novel approach
% for ultrafast imaging technique', in IEEE International Ultrasound Symposium 2014.
% Author: Olivier Bernard
% CREATIS laboratory, INSA Lyon
% email address: olivier.bernard@insa-lyon.fr  
% August 2016
    settings = param.settings;
    %---------------------------------------------------------------------
    %-- Fix acquisition Fourier space limits  
    flag_ximax = 0;
    flag_dxi = 0;
    flag_fftRF = 0;
    if exist('settings','var')       
        if isfield(settings,'ximax') 
            ximax = settings.ximax;
            %disp(['ximax = ',num2str(rad2deg(ximax))])
            flag_ximax = 1; 
        end
        if isfield(settings,'dxi') 
            dxi = settings.dxi;
            %disp(['dxi = ',num2str(rad2deg(dxi))])
            flag_dxi = 1; 
        end
        if isfield(settings,'flagfftRF') 
            flag_fftRF = settings.flagfftRF;
            %disp('Compute fftRF')
        end  
        if isfield(settings,'imageWidth') 
            imageWidth = settings.imageWidth;
        end                
        if isfield(settings,'imageDepth') 
            imageDepth = settings.imageDepth;
        end                
    end
    
    
    %---------------------------------------------------------------------     
    %-- Fix the reconstructed image size
    if exist('imageWidth','var') & exist('imageDepth','var')
         nx = ceil(imageWidth/param.Pitch);
         nz = ceil(imageDepth/(param.c0/(2*param.fs)))+1;
    else
         [nz,nx] = size(SIG(:,:,1)); 
    end
    
    
    %--------------------------------------------------------------------- 
    %-- Fix acquisition Fourier space limits
    if (flag_ximax==0)
        B = 2*param.bandwidth(1)/param.c0;
        ximax = 2 * atan(1/2/param.Pitch/B);
        %disp(['ximax = ',num2str(rad2deg(ximax))])
    end
    
    
    %--------------------------------------------------------------------- 
    %-- Parameter check
    interpolation = 'linear';
    c = param.c0;
    angles = param.TXangle;

   
    %--------------------------------------------------------------------- 
    %-- Fix Object Fourier Space limits
    %-- Compute dx and then nx
    dx = param.Pitch;
    if rem(nx,2)==1         %-- nx must be even
        nx = nx+1;
    end  
    nxFFT = 4*nx;           %-- zero padding in x to push away the side artefact 
    
    
    %--------------------------------------------------------------------- 
    %-- Compute dz and then nz
    dz = c/(2*param.fs); 
    nzFFT = 4*(nz-1)+1;      %-- zero padding in z to push away the side artefact

    
    %--------------------------------------------------------------------- 
    %-- Define K-space and frequency sampling
    kx = [0:nxFFT/2 -nxFFT/2+1:-1]/dx/nxFFT;   %-- The way Damien codes his functions
    kz = [0:(nzFFT-1)/2 -(nzFFT+1)/2+1:-1]/dz/nzFFT;
    freq = linspace(0,param.fs/2,(nzFFT-1)/2+1);
    k = freq/c;
    [Kx,Kz] = meshgrid(kx,kz);
    
    
    %--------------------------------------------------------------------- 
    %-- Fix acquisition Fourier space dxi
    if (flag_dxi==0)
        fov = 2*ximax/2;
        k1 = param.bandwidth(1)*2/param.c0;
        k2 = param.bandwidth(2)*2/param.c0;
        Nr = length(find(kz>k1 & kz<k2));
        dkx = kx(2)-kx(1);
        dkz = kz(2)-kz(1);
        dxi = 2 * fov / ( Nr / (dkx/dkz) );
        %disp(['dxi = ',num2str(rad2deg(dxi))])
    end    
    nxiFull = ceil(2*ximax/dxi)+1; 
    
    
    %---------------------------------------------------------------------     
    %-- Loop for each steered plane wave
    fftCmp = [];
    NbPW = size(SIG, 3);
    t0 = param.t0;    
    for its=1:NbPW    
    
        %---------------------------------------------------------------------
        %-- Get current rawdata and angle
        rawdata = SIG(:,:,its);
        dt = t0(its);
        if (length(angles)==1)
            angle = angles;
        else
            angle = angles(its);
        end
        
        %---------------------------------------------------------------------
        %-- Update the xi range according to the emitted angle value
        xi = linspace(-ximax+angle,ximax+angle,nxiFull);
        nxi = length(xi);
        
        %---------------------------------------------------------------------
        %-- Compute the Fourier projections
        S = zeros((nzFFT-1)/2+1,nxi);
        RF = fft(rawdata,nzFFT);
        
        %---------------------------------------------------------------------
        %-- The signal is real: only the positive temporal frequencies are kept
        RF = RF(1:((nzFFT-1)/2+1),:);
        for u=1:numel(xi)
            tau = (param.xm * sin(xi(u)) / c) - dt;
            delay = exp(-2i*pi*freq'*tau);
            S(:,u) = sum(RF.*delay,2);
        end

        %---------------------------------------------------------------------
        %-- Perform reconstruction
        cosA = cos(angle);
        sinA = sin(angle);
        K = (Kx.^2 + Kz.^2)/2./(Kx*sinA+Kz*cosA);
        Xi = atan2(2*Kx.*Kz*cosA+(Kx.^2-Kz.^2)*sinA,(Kz.^2-Kx.^2)*cosA+2*Kx.*Kz*sinA);     
        K(isnan(K))=0;
        
        %---------------------------------------------------------------------
        %-- Fill the object Fourier space
        fftAcqu = interp2(xi,k,S,Xi,K,interpolation,0);
       
        %---------------------------------------------------------------------
        %-- Update the Compound Fourier space
        if (its==1)
            fftCmp = fftAcqu;
        else
            fftCmp = fftCmp + fftAcqu;
        end        
        %disp(['Internal loop: ',num2str(its),' / ',num2str(NbPW)])   
     
    end
    
    
    %---------------------------------------------------------------------
    %-- Average fourier spectrum
    if (NbPW>1)
        fftCmp = fftCmp / NbPW;
    end    

    
    %---------------------------------------------------------------------
    %-- if nzFFT is odd
    if (flag_fftRF)
        fftCmp((nzFFT-1)/2+2:end,:) = conj([fftCmp((nzFFT-1)/2+1:-1:2,1) fftCmp((nzFFT-1)/2+1:-1:2,end:-1:2)]);
        fftCmp(1,end:-1:nxFFT/2+2) = conj(fftCmp(1,2:nxFFT/2));
        fftCmp(1) = 0;
        fftCmp(1,nxFFT/2+1) = 0;
    end
       
    
    %---------------------------------------------------------------------
    %-- Final regridded rf image
    rf_gridding = ifft2(fftCmp);

    
    %---------------------------------------------------------------------
    %-- Remove zero padding in both x and z (and eventually nx depending on flag_nx)
    if rem(nx,2)==1
        rf_gridding = [rf_gridding(1:nz,end-floor(nx/2)+1:end) rf_gridding(1:nz,1:ceil(nx/2))];
    else
        rf_gridding = [rf_gridding(1:nz,end-nx/2+1:end) rf_gridding(1:nz,1:nx/2+0)];
    end
    %---------------------------------------------------------------------
    %-- Grid coordinates
    x =((0:nx-1)-(nx-1)/2)*dx;
    z = ((0:nz-1))*dz;  
    
    %---------------------------------------------------------------------
    %-- Compute the corresponding enveloppe and bmode images without log compression
    env = abs(hilbert(rf_gridding));    
    bmode = env/max(max(env));
end
