function [bmode,migSIG,param] = lumig_sparse(SIG,param, transform)
%lumig_sparse - Reconstruct ultrasound image from sparse regularization approach with lumig
%
% Syntax:  [bmode,migSIG,param] = lumig_sparse(SIG,param,transform)
%
% Inputs:
%    SIG - element raw data (stored as a 3D matrix (number of lines * number of transducers * number of frames))
%    param - structure containing various parameter related to the probe, medium (cf. lumig.m for more details)
%    transform - structure which contains the parameter related to the sparsifying transform:
%			- transform.wtransform: type of the sparsifying trasnform. It could be 'sara', 'orthogonalwavelet', 'undecimatedwavelet' or 'dirac'
%			- transform.motherfunction: Mother function for the wavelet-based models
%			- transform.level: level of the decomposition for the wavelet-based models
%			- transform.beta: sparsity promoting parameter (between 0 and 1)
% Outputs:
%    bmode - bmode image (normalized envelope)
%    migSIG - RF image
%	 param - structure which contains details related to the image, medium and probe
%
%
% Other m-files required: Fessler's irt toolbox needed (for the non-uniform Fourier transform) 
% Subfunctions: none
%
% See also: lumig

% Author: Adrien Besson
% Signal processing laboratory (LTS5), Ecole polytechnique Federale de Lausanne
% email address: adrien.besson@epfl.ch  
% August 2016

[nt,nx,Nframes] = size(SIG);

%----- Input parameters -----
%-- 1) Speed of sound
if ~isfield(param,'c')
    param.c = 1540; % longitudinal velocity in m/s
end
c = param.c;
%-- 2) Sample frequency
if ~isfield(param,'fs')
    error(['A sample frequency (fs) ',...
        'must be specified as a structure field.'])
end
%-- 3) Acquisition start time
if ~isfield(param,'t0') % in s
    param.t0 = zeros(1,Nframes); % acquisition time start in s
end
if isscalar(param.t0)
    param.t0 = param.t0*ones(1,Nframes);
end
assert(numel(param.t0)==Nframes,...
    'PARAM.t0 must be a scalar or a vector of length number_of_frames.')
%-- 4) Pitch
if ~isfield(param,'pitch') % in m
    error('A pitch value (PARAM.pitch) is required.')
end
%-- 5) Transmit angle
if ~isfield(param,'TXangle')
    param.TXangle = 0; % in rad
end
if isscalar(param.TXangle)
    param.TXangle = param.TXangle*ones(1,Nframes);
end
assert(numel(param.TXangle)==Nframes,...
    'PARAM.TXangle must be a scalar or a vector of length number_of_frames.')
%-- 6) Transform
if (numel(fieldnames(transform)) < 4)
    if (~isfield(transform,'wtranform'))
        err('Please specify the field transform.wtransform with: dirac, orthogonalwavelet, undecimatedwavelet, sara');
    end
    if(~isfield(transform,'level'))
        %-- Case of a Dirac basis, no need for level
        if (~strcmp(lower(transform.wtransform), 'dirac'))
            disp('No decomposition level (trasnform.level) specified for wavelet -> default value set to 1');
            transform.level = 1;
        end
    end
    if(~isfield(transform,'motherfunction'))
        %-- Case of a Dirac basis, no need for filter
        if (~strcmp(lower(transform.wtransform), 'dirac'))
            disp('No mother function (transform.motherfunction) specified for wavelet -> default value set to db4');
            transform.motherfunction = 'db4';
        end
    end
    if(~isfield(transform,'beta'))
        disp('No sparsity promoting parameter (transform.beta) specified -> default value set to 0.5');
        transform.beta = 0.5;
    else
        if (transform.beta <0 || transform.beta > 1)
            err('Wrong value for the  sparsity promoting parameter: it must be between 0 and 1');
        end
    end
end
%----- end of Input parameters -----


SIG = double(SIG);


%-- Temporal shift
t0 = param.t0;
ntshift = max(round(t0*param.fs));
dx = param.pitch;

%-- Zero-padding before FFTs
ntFFT = 2*nt+ntshift;
if rem(ntFFT,2)==1 % ntFFT must be even
    ntFFT = ntFFT+1;
end
nxFFT = nx; % in order to avoid lateral edge effects
if rem(nxFFT,2)==1 % nxFFT must be even
    nxFFT = nxFFT+1;
end

%-- K-space coordinates
f0 = (0:ntFFT/2)'*param.fs/ntFFT;
kx = [0:nxFFT/2 -nxFFT/2+1:-1]/param.pitch/nxFFT;
[kx,f] = meshgrid(kx,f0);

%Initialize measurement vector and matrix (CS problem)
y = [];
G = [];
disp('********** Creation of the measurement matrix **********')
for k = 1:Nframes
    
    %-- Temporal FFT
    SIGk = fft(SIG(:,:,k),ntFFT);
    % The signal is real: only the positive temporal frequencies are kept:
    SIGk(ntFFT/2+2:ntFFT,:,:) = [];
    
    sinA = sin(param.TXangle(k));
    cosA = cos(param.TXangle(k));
    
%     %-- Compensate for steering angle and/or depth start
    if sinA~=0 || t0(k)~=0
        dt = sinA*((nx-1)*(param.TXangle(k)<0)-(0:nx-1))*...
            param.pitch/param.c; % steering angle
        tmp = bsxfun(@times,f0,dt+t0(k)); % depth start
        SIGk = SIGk.*exp(-2*1i*pi*tmp);
    end
    
    %-- Spatial FFT
    SIGk = fft(SIGk,nxFFT,2);
    
    % Creation of the non-uniform K-space
    ki = f/param.c;
    KLu = ki*cosA + sqrt(ki.^2*cosA^2-kx.^2 + 2*ki.*kx*sinA);
    KLu = real(KLu.*(KLu > 0));
    Kx1= kx(:)*2*pi*dx;
    Kz1 = KLu(:)*pi*param.c/(param.fs);
	[Nz Nx] = size(kx);
 
    %Set measurement operators and data for sparse reconstruction
    y = [y; SIGk(:)];
    G1 = Gnufft({[Kz1(:) Kx1(:)],[Nz Nx], [8, 8], [2*Nz 2*Nx], [0 0]});
    G = [G; G1];
end
% Measurement operators for the CS-based algorithm    
A = @(x) G*reshape(x, Nz*Nx, 1);
At = @(x) reshape(G'*x, Nz, Nx);

%Calculation of the norm of AtA (used in the algorithm)
disp('********** Calculaton of the norm of the measurement matrix (power method) **********')
eval = pow_method(A, At, [Nz,Nx], 1e-3, 100, 0);  

% Measurement operators for the CS-based algorithm    
A = @(x) A(x) / sqrt(eval);
At = @(x) At(x) / sqrt(eval);

%Sparsifying frames for the CS reconstruction
sol1 = At(y);
switch lower(transform.wtransform)
    case 'sara'
        dwtmode('per');
        [C,S]=wavedec2(sol1,transform.level,'db8');
        ncoef=length(C);
        [C1,S1]=wavedec2(sol1,transform.level,'db1');
        ncoef1=length(C1);
        [C2,S2]=wavedec2(sol1,transform.level,'db2');
        ncoef2=length(C2);
        [C3,S3]=wavedec2(sol1,transform.level,'db3');
        ncoef3=length(C3);
        [C4,S4]=wavedec2(sol1,transform.level,'db4');
        ncoef4=length(C4);
        [C5,S5]=wavedec2(sol1,transform.level,'db5');
        ncoef5=length(C5);
        [C6,S6]=wavedec2(sol1,transform.level,'db6');
        ncoef6=length(C6);
        [C7,S7]=wavedec2(sol1,transform.level,'db7');
        ncoef7=length(C7);
        
        %Sparsity averaging operator
        Psit = @(x) [wavedec2(x,transform.level,'db1')'; wavedec2(x,transform.level,'db2')';wavedec2(x,transform.level,'db3')';...
            wavedec2(x,transform.level,'db4')'; wavedec2(x,transform.level,'db5')'; wavedec2(x,transform.level,'db6')';...
            wavedec2(x,transform.level,'db7')';wavedec2(x,transform.level,'db8')']/sqrt(8);
        
        Psi = @(x) (waverec2(x(1:ncoef1),S1,'db1')+waverec2(x(ncoef1+1:ncoef1+ncoef2),S2,'db2')+...
            waverec2(x(ncoef1+ncoef2+1:ncoef1+ncoef2+ncoef3),S3,'db3')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+1:ncoef1+ncoef2+ncoef3+ncoef4),S4,'db4')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+ncoef4+1:ncoef1+ncoef2+ncoef3+ncoef4+ncoef5),S5,'db5')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+1:ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6),S6,'db6')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6+1:ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6+ncoef7),S7,'db7')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6+ncoef7+1:ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6+ncoef7+ncoef),S,'db8'))/sqrt(8);
        
    case 'orthogonalwavelet'
        [~,S]=wavedec2(sol1,transform.level,transform.motherfunction);
        Psit = @(x) odwt2(x, transform.level, transform.motherfunction);
        Psi = @(x) iodwt2(x, S, transform.motherfunction);
        
    case 'undecimatedwavelet'
        Wc = swt2(sol1,transform.level,transform.motherfunction);
        Psit = @(x) udwt2(x, transform.level,transform.motherfunction);
        Psi = @(x) iudwt2(x, Wc, transform.motherfunction);
    case 'dirac'
        Psit = @(x) x;
        Psi = @(x) x;
    otherwise
        err('Wrong sparsifying model: please specify transform.wtransform with sara, orthogonalwavelet, undecimatedwavelet or dirac');
end

%Parameters for admm solver
ss = Psit(sol1);
lambda = 1e-2*norm(abs(ss(:)),inf);
epsilon = transform.beta*norm(y(:));
param1.verbose = 1; % Print log or not
param1.rel_obj = 1e-4; % Stopping criterion for the L1 problem
param1.gamma = lambda;
if (isfield(param, 'max_iter'))
   param1.max_iter = param.max_iter;
else
   param1.max_iter = 200; % Max. number of iterations for the L1 problem
end
% param1.nu = eval; % Bound on the norm of the operator A
param1.tight_L1 = 1; % Indicate if Psit is a tight frame (1) or not (0)
param1.max_iter_L1 = 10;
param1.rel_obj_L1 = 1e-2;
param1.pos_L1 = 0;
param1.nu_L1 = 1;
param1.verbose_L1 = 0;
param1.initsol = sol1;

% Sparse Regularization using ADMM
disp('********** Sparse regularization **********')
if param.max_iter > 0
    [SIGk, ~] = admm_bpcon(y, epsilon, A, At, Psi, Psit, param1);
else
    SIGk = sol1;
end
migSIG = SIGk((1:nt),1:nx);

%-- Grid coordinates
if nargin>1
param.x = ((0:nx-1)-(nx-1)/2)*param.pitch;
param.z = ((0:nt-1))*param.c/2/param.fs;
end

%-- Compute the corresponding enveloppe image
migSIG = migSIG(1:nt,1:nx);
hilb = hilbert(migSIG);
env = abs(hilb);
bmode = env/max(max(env));

end

