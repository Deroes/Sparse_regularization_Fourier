function coef = udwt2(x,level,filter)
%Multilevel Undecimated wavelet decomposition


W1 = swt2(x,level,filter);

[N3, N2, N1]=size(W1);
coef = [];

for i = 1:N1
    coef = [coef(:); reshape(W1(:,:,i), N3*N2, 1)];
end

end

