% Script used to reproduce to test the approach proposed in the paper:
% A. Besson, M. Zhang, F. Varray,  H. Liebgott, D. Friboulet, Y. Wiaux, J.-P. Thiran, R. E. Carrillo and O. Bernard, "A sparse regularization framework for Fourier-based plane wave imaging" accepted to IEEE Transcations on Ultrasonics, Ferroelectrics, and
% Frequency Control

clear all;
close all;
clc;
addpath(genpath('src'));
run 'irt/setup.m'

%% User-defined parameters
%-- Input parameters for the methods
number_plane_waves = 1;					% You can change this parameter: 1, 3, 5 PWs are available
beta_ufsb = 0.39;						% Sparsity promoting parameter for ufsb -> must be between 0 and 1
beta_lu = 0.2;							% Sparsity promoting parameter for lu -> must be between 0 and 1
beta_garcia = 0.5;						% Sparsity promoting parameter for garcia -> must be between 0 and 1
max_iterations = 200;					% Maximum number of iterations

%-- Parameters of the sparsifying transform
transform.wtransform = 'SARA';			% Sparsifying model: 'SARA', 'dirac', 'orthogonalwavelet', 'undecimated wavelet'
transform.wfilter = 'db4';				% Wavelet mother function
transform.level = 1;					% Level of the decomposition

%% Preprocess the data according to the number of plane waves

%-- Load the data 
load_file = 'data/rawdata_cyst_inclusion_5PW.mat';
load(load_file);
probe.pitch = probe.Pitch;
probe.c = probe.c0;
probe.max_iter = max_iterations;
%-- Preprocess the data
[rf_data, probe] = select_data(data, steeringAngles, number_plane_waves, probe);

%-- Parameters used for the calculation of the contrast
xc = 0/1000;
zc = 40/1000;
r = 8/2/1000;
fact2 = 0.75;

%-- Generate image grid
x = probe.xm;
z = (0:size(rf_data, 1)-1)* probe.c0 / probe.fs / 2;

%% Sparse UFSB
probe.settings = struct('flagfftRF',1, 'dxi', deg2rad(0.5), 'ximax', deg2rad(50));
transform.beta = beta_ufsb;
[bmode_ufsb, migSIG_ufsb, param2] = ufsb_sparse(rf_data, probe, transform);
x_ufsb = param2.x;
z_ufsb = param2.z;

%---------------------------------------------------------------------
%-- Compute contrast
parameters = struct('x',x_ufsb,'z',z_ufsb,'xc',xc,'zc',zc,'r',r,'fact2',fact2);
[~,cr2_bernard,~] = EvaluateContrasts(bmode_ufsb,parameters);


%% Sparse Garcia
probe.t0 = 0;
transform.beta = beta_garcia;
[bmode_garcia, migSIG_garcia, param2] = fkmig_sparse(rf_data,probe, transform);
x_garcia = param2.x;
z_garcia = param2.z;

%---------------------------------------------------------------------
%-- Compute contrast
parameters = struct('x',x_garcia,'z',z_garcia,'xc',xc,'zc',zc,'r',r,'fact2',fact2);
[~,cr2_garcia,~] = EvaluateContrasts(bmode_garcia,parameters);


%% Sparse Lu
transform.beta = beta_lu;
[bmode_lu, migSIG_lu, param2] = lumig_sparse(rf_data,probe, transform);
x_lu = param2.x;
z_lu = param2.z;

%---------------------------------------------------------------------
%-- Compute contrast
parameters = struct('x',x_lu,'z',z_lu,'xc',xc,'zc',zc,'r',r,'fact2',fact2);
[~,cr2_lu,info] = EvaluateContrasts(bmode_lu,parameters);


%% Display
dBRange = 40;

%-- Range of the displayed area
minX = -12.5/1000;
maxX = 12.5/1000;
minZ = 5/1000;
maxZ = 55/1000;

%-- UFSB
h = figure;
set(h,'Color',[1 1 1]);
bmode = ComputeLogCompressedBmode(bmode_ufsb);
imagesc(x_ufsb*1000,z_ufsb*1000,bmode); colormap(gray); caxis([-dBRange 0]);
axis image; axis([minX maxX minZ maxZ]*1000);
xlabel('Lateral position [mm]');
ylabel('Depth [mm]');
title(['Sparse UFSB | CR2 = ',num2str(cr2_bernard), ' dB']);
h2 = colorbar;
set(h2,'YTick',[-40 -30 -20 -10 0]);
set(h2,'YTickLabel',{'-40 dB';'-30 dB';'-20 dB';'-10 dB';'0 dB'});
set(findall(h,'type','text'),'FontSize',11,'fontWeight','bold');
ha = get(h,'CurrentAxes');
set(ha,'FontSize',12,'fontWeight','bold');

%-- Garcia
h = figure;
set(h,'Color',[1 1 1]);
bmode = ComputeLogCompressedBmode(bmode_garcia);
imagesc(x_garcia*1000,z_garcia*1000,bmode); colormap(gray); caxis([-dBRange 0]);
axis image; axis([minX maxX minZ maxZ]*1000);
xlabel('Lateral position [mm]');
ylabel('Depth [mm]');
title(['Garcia: | CR2 = ',num2str(cr2_garcia), ' dB']);
h2 = colorbar;
set(h2,'YTick',[-40 -30 -20 -10 0]);
set(h2,'YTickLabel',{'-40 dB';'-30 dB';'-20 dB';'-10 dB';'0 dB'});
set(findall(h,'type','text'),'FontSize',11,'fontWeight','bold');
ha = get(h,'CurrentAxes');
set(ha,'FontSize',12,'fontWeight','bold');

%-- Lu
h = figure;
set(h,'Color',[1 1 1]);
bmode = ComputeLogCompressedBmode(bmode_lu);
imagesc(x_lu*1000,z_lu*1000,bmode); colormap(gray); caxis([-dBRange 0]);
axis image; axis([minX maxX minZ maxZ]*1000);
xlabel('Lateral position [mm]');
ylabel('Depth [mm]');
title(['Lu: | CR2 = ',num2str(cr2_lu)]);
h2 = colorbar;
set(h2,'YTick',[-40 -30 -20 -10 0]);
set(h2,'YTickLabel',{'-40 dB';'-30 dB';'-20 dB';'-10 dB';'0 dB'});
set(findall(h,'type','text'),'FontSize',11,'fontWeight','bold');
ha = get(h,'CurrentAxes');
set(ha,'FontSize',12,'fontWeight','bold');
        
